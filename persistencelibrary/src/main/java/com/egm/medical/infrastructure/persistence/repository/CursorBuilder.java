package com.egm.medical.infrastructure.persistence.repository;

import android.database.Cursor;

public interface CursorBuilder<T> {

    T build(Cursor c);

}
