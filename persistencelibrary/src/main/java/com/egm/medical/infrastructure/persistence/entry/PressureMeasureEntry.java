package com.egm.medical.infrastructure.persistence.entry;

public class PressureMeasureEntry {

    public static final String TABLE_NAME = "PRESSURE_MEASURE";
    public static final String COLUMN_MEASURE_DATE = "measure_date";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_SYSTOLIC = "systolic";
    public static final String COLUMN_DIASTOLIC = "diastolic";
    public static final String COLUMN_PULSE = "pulse";
    public static final String COLUMN_LOCATION = "location";
    public static final String COLUMN_POSITION = "position";

    public static final String SELECTION_MEASURE_DATE = COLUMN_MEASURE_DATE + "=?";

    public static String[] prepareProjections() {
        return new String[]{COLUMN_MEASURE_DATE,
                COLUMN_DESCRIPTION,
                COLUMN_SYSTOLIC,
                COLUMN_DIASTOLIC,
                COLUMN_PULSE,
                COLUMN_LOCATION,
                COLUMN_POSITION};
    }

    public static String prepareOrderBy() {
        return COLUMN_MEASURE_DATE;
    }

}
