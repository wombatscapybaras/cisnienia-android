package com.egm.cisnienia_android;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.egm.medical.domain.model.blood_pressure.PressureMeasure;
import com.egm.medical.infrastructure.persistence.repository.SQLLittlePressureMeasureRepository;

import org.joda.time.DateTime;

import java.util.List;

public class PressureMeasureDetailsActivity extends AppCompatActivity {

    public static final String EXTRA_PRESSURE_MEASURE_DATE = "com.egm.cisnienia_android.pm_date";

    private ViewPager mViewPager;
    private List<PressureMeasure> mPressureMeasures;

    public static Intent newIntent(Context context, DateTime measureDate) {
        Intent intent = new Intent(context, PressureMeasureDetailsActivity.class);
        intent.putExtra(EXTRA_PRESSURE_MEASURE_DATE, measureDate);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pressure_measure_pager);

        mViewPager = (ViewPager) findViewById(R.id.activity_pressure_measure_pager);

        final DateTime measureDate = (DateTime) getIntent().getSerializableExtra(EXTRA_PRESSURE_MEASURE_DATE);

        mPressureMeasures = new SQLLittlePressureMeasureRepository().getAll();

        FragmentManager fragmentManager = getSupportFragmentManager();
        mViewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position) {
                PressureMeasure pm = mPressureMeasures.get(position);
                return PressureMeasureDetailsFragment.newInstance(pm.getMeasureDate());
            }

            @Override
            public int getCount() {
                return mPressureMeasures.size();
            }
        });

        setViewPagerItem(measureDate);

    }

    private void setViewPagerItem(DateTime measureDate) {
        for (int i = 0; i <= mPressureMeasures.size(); i++) {
            if (mPressureMeasures.get(i).getMeasureDate().isEqual(measureDate)) {
                mViewPager.setCurrentItem(i);
                break;
            }
        }
    }

}
