package com.egm.cisnienia_android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.egm.medical.domain.model.blood_pressure.BPRange;
import com.egm.medical.domain.model.blood_pressure.BloodPressure;
import com.egm.medical.domain.model.blood_pressure.PressureMeasure;
import com.egm.medical.infrastructure.persistence.repository.SQLLittlePressureMeasureRepository;
import com.google.common.base.Predicate;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import static com.egm.medical.domain.model.blood_pressure.BPRange.*;

public class PressureMeasureListFragment extends Fragment {

    private static final DateFormat fmt = new SimpleDateFormat("dd MMM yyyy dd HH:mm");

    private static final int REQUEST_MEASURE_ADD = 1;
    private static final int REQUEST_MEASURE_DETAILS = 2;
    private PressureMeasureAdapter mPressureMeasureAdapter;
    private RecyclerView mRecyclerView;

    public static PressureMeasureListFragment newInstance() {
        return new PressureMeasureListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(Boolean.TRUE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pressure_measure_list, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.blood_pressure_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        updateList();

        return view;
    }

    private class PressureMeasureHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private PressureMeasure mPressureMeasure;

        public TextView mDate;
        public TextView mSystolic;
        public TextView mDiastolic;
        public TextView mPulse;
        public ImageView mMoodIcon;

        public PressureMeasureHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mDate = (TextView) itemView.findViewById(R.id.list_item_pressure_measure_date);
            mSystolic = (TextView) itemView.findViewById(R.id.list_item_pressure_measure_systolic);
            mDiastolic = (TextView) itemView.findViewById(R.id.list_item_pressure_measure_diastolic);
            mPulse = (TextView) itemView.findViewById(R.id.list_item_pressure_measure_pulse);
            mMoodIcon = (ImageView) itemView.findViewById(R.id.mood_icon);
        }


        public void bindPressureMeasure(PressureMeasure pressureMeasure) {
            mPressureMeasure = pressureMeasure;
            mDate.setText(fmt.format(mPressureMeasure.getMeasureDate().toDate()));
            mSystolic.setText(String.valueOf(mPressureMeasure.getBloodPressure().systolic()));
            mDiastolic.setText(String.valueOf(mPressureMeasure.getBloodPressure().diastolic()));
            mPulse.setText(String.valueOf(mPressureMeasure.getBloodPressure().pulse()));

            if(isBloodPressureInRange(highBPRange()))
                mMoodIcon.setImageResource(R.drawable.yellow);
            else if(isBloodPressureInRange(preHighBPRange()))
                mMoodIcon.setImageResource(R.drawable.yellow);
            else if(isBloodPressureInRange(lowBPRange()))
                mMoodIcon.setImageResource(R.drawable.yellow);
            else if(isBloodPressureInRange(idealBP()))
                mMoodIcon.setImageResource(R.drawable.green);
        }

        public boolean isBloodPressureInRange(Predicate<BloodPressure> range) {
            return mPressureMeasure.getBloodPressure().isInRange(range);
        }

        @Override
        public void onClick(View v) {
            Intent intent = PressureMeasureDetailsActivity.newIntent(getActivity(), mPressureMeasure.getMeasureDate());
            startActivityForResult(intent, REQUEST_MEASURE_DETAILS);
        }
    }

    private class PressureMeasureAdapter extends RecyclerView.Adapter<PressureMeasureHolder> {

        private List<PressureMeasure> pressureMeasures;

        public PressureMeasureAdapter(List<PressureMeasure> pressureMeasures) {
            this.pressureMeasures = pressureMeasures;
        }

        @Override
        public PressureMeasureHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater li = LayoutInflater.from(getActivity());
            View view = li.inflate(R.layout.list_item_pressure_measure, parent, false);
            return new PressureMeasureHolder(view);
        }

        @Override
        public void onBindViewHolder(PressureMeasureHolder holder, int position) {
            PressureMeasure pm = pressureMeasures.get(position);
            holder.bindPressureMeasure(pm);
        }

        @Override
        public int getItemCount() {
            return pressureMeasures.size();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_pressure_measure_list, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_new_measure:
                Intent intent = new Intent(getActivity(), PressureMeasureAddActivity.class);
                startActivityForResult(intent, REQUEST_MEASURE_ADD);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK)
            return;

        if (requestCode == REQUEST_MEASURE_ADD)
            Toast.makeText(getActivity(), "Dodano pomiar!", Toast.LENGTH_SHORT).show();

        if (requestCode == REQUEST_MEASURE_DETAILS)
            Toast.makeText(getActivity(), "Usunięto pomiar!", Toast.LENGTH_LONG).show();

        updateList();
    }

    private void updateList() {
        mPressureMeasureAdapter = new PressureMeasureAdapter(new SQLLittlePressureMeasureRepository().getAll());
        mRecyclerView.setAdapter(mPressureMeasureAdapter);
    }
}
