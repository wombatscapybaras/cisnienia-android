package com.egm.cisnienia_android;

import android.app.Activity;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.egm.medical.domain.model.blood_pressure.PressureMeasure;
import com.egm.medical.infrastructure.persistence.repository.SQLLittlePressureMeasureRepository;

import org.joda.time.DateTime;

public class PressureMeasureDetailsFragment extends Fragment {

    public static final String ARG_PRESSURE_MEASURE_DATE = "pm_date";

    private PressureMeasure mPressureMeasure;
    private Toolbar mToolbar;

    public static PressureMeasureDetailsFragment newInstance(DateTime measureTime) {
        PressureMeasureDetailsFragment fragment = new PressureMeasureDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PRESSURE_MEASURE_DATE, measureTime);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(Boolean.TRUE);

        DateTime measureTime = (DateTime) getArguments().getSerializable(ARG_PRESSURE_MEASURE_DATE);
        mPressureMeasure = new SQLLittlePressureMeasureRepository().getByMeasureDate(measureTime);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pressure_measure_details, container, false);

        initToolbar(view);

        TextView mSystolicField = (TextView) view.findViewById(R.id.details_item_systolic);
        mSystolicField.setText(String.valueOf(mPressureMeasure.getBloodPressure().systolic()));

        TextView mDiastolicField = (TextView) view.findViewById(R.id.details_item_diastolic);
        mDiastolicField.setText(String.valueOf(mPressureMeasure.getBloodPressure().diastolic()));

        TextView mPulseField = (TextView) view.findViewById(R.id.details_item_pulse);
        mPulseField.setText(String.valueOf(mPressureMeasure.getBloodPressure().pulse()));

        Button mDeleteMeasureButton = (Button) view.findViewById(R.id.button_delete_measure);

        mDeleteMeasureButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                deleteMeasure();
            }
        });

        return view;
    }

    private void initToolbar(View view) {
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(mToolbar);
        activity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_pressure_measure_details, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_delete_measure:
                deleteMeasure();
                return Boolean.TRUE;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    private void deleteMeasure() {
        new SQLLittlePressureMeasureRepository().deleteByMeasureDate(mPressureMeasure.getMeasureDate());

        getActivity().setResult(Activity.RESULT_OK);
        getActivity().finish();
    }
}
