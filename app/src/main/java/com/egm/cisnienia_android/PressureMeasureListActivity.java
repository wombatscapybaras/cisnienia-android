package com.egm.cisnienia_android;

import android.support.v4.app.Fragment;


public class PressureMeasureListActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return PressureMeasureListFragment.newInstance();
    }

}
